Colophon
========

This website based on the Grayscale Bootstrap theme:
http://startbootstrap.com/template-overviews/grayscale/

The image of Mare Nostrum (Barcelona Supercomputer Center) modified from the original at:
http://commons.wikimedia.org/wiki/File:BSC-MareNostrum-E.JPG

The image of the Eniac modified from the original at:
http://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Eniac.jpg/1280px-Eniac.jpg

The image of the calendar modified from the original at:
http://upload.wikimedia.org/wikipedia/commons/6/61/Kiowa_Anko_calendar_on_buckskin_-_NARA_-_523631.jpg

# eof
