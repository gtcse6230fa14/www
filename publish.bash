#!/usr/bin/env bash

SITE=vuduc.org
CLASS_SHORT=cse6230
CLASS_LONG=${CLASS_SHORT}-hpcta-fa14
ROOT=public_html/teaching
FILES=""
FILES="${FILES} css"
FILES="${FILES} font-awesome-4.1.0"
FILES="${FILES} fonts"
FILES="${FILES} img"
FILES="${FILES} js"
FILES="${FILES} less"
FILES="${FILES} slides"
FILES="${FILES} index.html"

# Create directory
ssh ${SITE} "mkdir -p ${ROOT}/${CLASS_LONG}/"

# Sync files
rsync -avz -e ssh ${FILES} ${SITE}:${ROOT}/${CLASS_LONG}/.

if ! test -h ${CLASS_SHORT} ; then
  ln -s ${CLASS_LONG} ${CLASS_SHORT}
fi

# Create shortcut
if ! ssh ${SITE} test -h ${ROOT}/${CLASS_SHORT} ; then
  rsync -avz -e ssh ${CLASS_SHORT} ${SITE}:${ROOT}/${CLASS_SHORT}
fi

# eof
